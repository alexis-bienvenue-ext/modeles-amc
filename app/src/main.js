import { createApp } from 'vue'
import App from './App.vue'

import './assets/css/main.css';
import { simpleString } from './utils.js';

const amcApp = createApp(App);

// Load global data from JSON files
var req = ["tags", "versions", "templates"].map((k) =>
    fetch(`./${k}.json`)
        .then(response => response.json())
        .then(data => { amcApp.config.globalProperties['$' + k] = data })
);

function ready() {
    console.log(amcApp.config.globalProperties.$tags);
    // Remove accents in tags
    for (let t of amcApp.config.globalProperties.$tags) {
        t.key = simpleString(t.text);
    }
    // and in things to be searched
    for (let t of amcApp.config.globalProperties.$templates) {
        t.key = simpleString(t.title + " " + t.description);
        t.stags = t.tags.map((i) => simpleString(i));
    }
    amcApp.mount('#app');
}

// Wait for data to be loaded to mount the app on DOM (removing the "Loading…" warning)
Promise.all(req).then(() => { ready() } );





