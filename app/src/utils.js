
export function simpleString(s) {
    return s.normalize("NFKD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
}

export function versionArray(s) {
    if(s == "devel") {
        return([99]);
    } else {
        return s.split(".").map((i)=>parseInt(i));
    }
}

