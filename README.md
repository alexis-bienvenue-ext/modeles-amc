# Modèles pour AMC

## Présentation

Ce projet rassemble quelques modèles de fichiers sources pour
concevoir des QCM avec [Auto Multiple Choice](https://www.auto-multiple-choice.net).

Vous pouvez les consulter ici : https://alexis-bienvenue-ext.forge.apps.education.fr/modeles-amc

## Contribuer

Ce projet est ouvert aux contributions externes, que ce soit en
nouveaux modèles, ou bien en amélioration du code du site web.

### Pour proposer un nouveau modèle

Vous pouvez regarder dans le répertoire `templates` comment sont
construits les modèles déjà existants. Il vous faudra créer un nouveau
répertoire, avec le fichier source de votre modèle, un fichier
`template.yml` qui le décrit, et éventuellement quelques scans de
copies remplies, un fichier `options.xml` et/ou d'autres fichiers
nécessaires au fonctionnement du modèle.

N'hésitez pas à me contacter pour affiner votre proposition.

### Pour améliorer le site

Ce projet est ouvert aux demandes de fusion !
