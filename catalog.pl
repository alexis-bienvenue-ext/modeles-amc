#! /usr/bin/perl

use warnings;
use 5.012;

use YAML::Syck;
use JSON;

my $templates_dir = 'templates';

opendir( my $dh, $templates_dir ) || die "Can't opendir $templates_dir: $!";
my @template_names = grep { !/^\./ && -d "$templates_dir/$_" } readdir($dh);
closedir $dh;

$YAML::Syck::ImplicitTyping  = 1;
$YAML::Syck::ImplicitUnicode = 1;

my @templates = ();
my %tags      = ();

for my $t (@template_names) {
    my $f = "$templates_dir/$t/template.yml";
    -f $f or die "Missing yml description for template $t";
    my $desc = LoadFile($f);

    for my $k (qw/date title needs description/) {
        die "Missing $k for template $t" if ( !$desc->{$k} );
    }

    $desc->{author} = "" if(!$desc->{author});

    if ( $desc->{tags} ) {
        $desc->{tags} = [ $desc->{tags} ] if ( !ref( $desc->{tags} ) );
        for my $tag ( @{ $desc->{tags} } ) {
            $tags{$tag}++;
        }
    }

    $desc->{id} = $t;

    push @templates, $desc;
}

@templates = sort { $b->{date} cmp $a->{date} } @templates;

my @tags = map { { text => $_, count => $tags{$_} } }
  sort { $tags{$b} <=> $tags{$a} || $a cmp $b } ( keys %tags );

my $json_file = 'build/templates.json';

open JS, ">", $json_file or die "Can't write to $json_file: $!";
print JS encode_json( \@templates );
close JS;

my $tags_file = 'build/tags.json';

open TS, ">", $tags_file or die "Can't write to $tags_file: $!";
print TS encode_json( \@tags );
close TS;
