
TEMPLATES=$(notdir $(wildcard templates/*))

clean: FORCE
	-rm -r build/*
	-rm app/public/t/*

build: $(foreach T,$(TEMPLATES), $(T).build)
	./catalog.pl

%.build: FORCE
	./build.sh $*

prepare: FORCE
	test -d app/public/t || mkdir app/public/t
	cp build/t/* app/public/t
	cp build/templates.json build/tags.json app/public

FORCE: ;

