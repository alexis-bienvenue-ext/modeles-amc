#! /bin/bash

set -e

T=$1

BASE=`pwd`
TEMPLATES=$BASE/templates
BUILD=`mktemp -d`
RESULTS=$BASE/build/t

if [ ! "$T" ];
then
    echo "Template name missing"
    exit 1
fi

if [ ! -d $TEMPLATES/$T ];
then
    echo "Missing template directory $TEMPLATES/$T"
    exit 1
fi

AMC_OPTS="--profile-conf $BASE/cf.default.xml"

echo "DIR = $BUILD"

# create results directory if not present

test -d $RESULTS || mkdir -p $RESULTS

# build ZIP file with template

cd $TEMPLATES

ZIP="$RESULTS/$T.zip"
test -f $ZIP && rm $ZIP
zip $ZIP -r $T --exclude '*/build/*' --exclude '*/scans/*'

# install template in temp directory

cd $BUILD
unzip $ZIP

mkdir $BUILD/$T/data
mkdir -p $BUILD/$T/cr/corrections/pdf

# Build question

echo "=== Sujet ==="
auto-multiple-choice prepare $AMC_OPTS --project-dir $BUILD/$T --mode 's[]'

# Include question file as PDF and as PNG (first page)

cp $BUILD/$T/DOC-sujet.pdf $RESULTS/$T.pdf
gs -sDEVICE=png16m -sOutputFile=$RESULTS/$T.png -r200 -dNOPAUSE -dSAFER -dBATCH -dFirstPage=1 -dLastPage=1 -dTextAlphaBits=4 $RESULTS/$T.pdf

# Layout detection

auto-multiple-choice meptex $AMC_OPTS --project-dir $BUILD/$T

# Parse scoring stratagy

echo "=== Bareme ==="
auto-multiple-choice prepare $AMC_OPTS --project-dir $BUILD/$T --mode b

# If no scans are provided, create one with the individual corrected question

if [ ! -d $TEMPLATES/$T/scans ];
then

    echo "=== Correction ==="
    auto-multiple-choice prepare $AMC_OPTS --project-dir $BUILD/$T --mode k
    echo 1 > $BUILD/$T/nums
    auto-multiple-choice imprime $AMC_OPTS --project-dir $BUILD/$T --sujet "$BUILD/$T/DOC-indiv-solution.pdf" --methode file --output "$BUILD/$T/xx-%e.pdf" --fich-numeros $BUILD/$T/nums

    mkdir $BUILD/$T/scans
    mkdir $BUILD/fake
    gs -sDEVICE=png16m -sOutputFile=$BUILD/fake/xx-%04d.png -r200 -dNOPAUSE -dSAFER -dBATCH -dTextAlphaBits=4 $BUILD/$T/xx-0001.pdf
    for f in `ls $BUILD/fake`
    do
        ROT=$(( $RANDOM % 10 ))
        ROTS=$(( $RANDOM % 2 ))
        if [ $ROTS -eq 1 ];
        then
            ROT=0.$ROT
        else
            ROT=-0.$ROT
        fi
        convert $BUILD/fake/$f -rotate $ROT +noise Poisson -threshold 50% $BUILD/$T/scans/$f
    done

fi

# If scans are provided, copy them in th build dir

if [ -d $TEMPLATES/$T/scans ];
then

    cp -r $TEMPLATES/$T/scans $BUILD/$T/scans

fi

# Scan analysis

SLIST=$BUILD/scans-list

ls $BUILD/$T/scans/* > $SLIST

auto-multiple-choice read-pdfform $AMC_OPTS --list $SLIST --project-dir $BUILD/$T
auto-multiple-choice getimages $AMC_OPTS --list $SLIST --project-dir $BUILD/$T
auto-multiple-choice analyse $AMC_OPTS --liste-fichiers $SLIST --project-dir $BUILD/$T --pre-allocate 1

# Grading

auto-multiple-choice note $AMC_OPTS --project-dir $BUILD/$T

# Association

if grep listeetudiants $BUILD/$T/options.xml
then

    auto-multiple-choice association-auto --project-dir $BUILD/$T

fi

# Annotation

auto-multiple-choice annotate $AMC_OPTS --project-dir $BUILD/$T --pdf-dir $RESULTS --single-output $T-annotee.pdf
gs -sDEVICE=png16m -sOutputFile=$RESULTS/$T-annotee.png -r200 -dNOPAUSE -dSAFER -dBATCH -dTextAlphaBits=4 -dFirstPage=1 -dLastPage=1 $RESULTS/$T-annotee.pdf

# Ajout copie enseignant si disponible

if [ -f $BUILD/$T/scans/enseignant.pdf ];
then

    mv $RESULTS/$T-annotee.pdf $BUILD
    pdftk $BUILD/$T-annotee.pdf $BUILD/$T/scans/enseignant.pdf output $RESULTS/$T-annotee.pdf

fi

echo "$T built in $BUILD"
