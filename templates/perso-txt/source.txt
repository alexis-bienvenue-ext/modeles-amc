# AMC-TXT copies personnalisées

PaperSize: A4
Lang: FR

# Fichier contenant la liste des étudiants :
PreAssociation: liste.csv
# Nom de colonne qui servira d'identifiant unique dans cette liste :
PreAssociationKey: id
# On choisit d'écrire prénom et nom sur la copie,
# qui sont dans les colonnes "prenom" et "nom" :
PreAssociationName: \prenom{} \nom{}

Title: Modèle avec copies personnalisées

Presentation: Veuillez répondre aux questions
ci-dessous du mieux que vous pouvez.

* Quelle est la capitale du Cameroun ?
+ Yaoundé
- Douala
- Kribi

** Parmi les nombres suivants,
lesquels sont positifs ?
+ 2
- -2
+ 10
